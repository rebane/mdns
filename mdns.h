#ifndef _MDNS_H_
#define _MDNS_H_

#include <stdint.h>

#define MDNS_DEST_PORT 5353

void mdns_recv_udp(const void *recv_buffer, uint16_t len);
void mdns_send_answer_a(const char *name, uint32_t ip);
void mdns_send_answer_ptr(uint32_t ip, const char *name);

void mdns_recv_query_a(const void *recv_buffer, uint16_t len, uint16_t name);
void mdns_recv_query_ptr(const void *recv_buffer, uint16_t len, uint32_t ip);
void mdns_send(const void *send_buffer, uint16_t len);

#endif

