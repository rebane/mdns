#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/net.h>
#include <linux/if_tun.h>
#include <net/if_arp.h>
#include <arpa/inet.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include "dns.h"
#include "mdns.h"

static char *interface_name, *server_name;
static int fd;

static int iface_addr_get(const char *iface, in_addr_t *addr);
static int iface_mac_get(const char *iface, void *mac);

int main(int argc, char *argv[]){
	int i, l;
	socklen_t len;
	unsigned char buffer[1024];
	char mac_name[32];
	struct sockaddr_in addr;
	struct ip_mreq mreq;

	if(argc < 2){
		printf("use: %s <interface> [name]\n", argv[0]);
		return(-1);
	}

	interface_name = argv[1];

	if(argc < 3){
		if(iface_mac_get(interface_name, buffer) < 0){
			printf("MAC GET\n");
			return(-1);
		}
		snprintf(mac_name, 31, "%02X%02X%02X%02X%02X%02X.local",
			(unsigned int)buffer[0],
			(unsigned int)buffer[1],
			(unsigned int)buffer[2],
			(unsigned int)buffer[3],
			(unsigned int)buffer[4],
			(unsigned int)buffer[5]
		);
		mac_name[31] = 0;
		printf("NAME: %s\n", mac_name);
		server_name = mac_name;
	}else{
		server_name = argv[2];
	}

	fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(fd < 0)return(-1);
	i = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i));
	setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &i, sizeof(i));
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(MDNS_DEST_PORT);
	addr.sin_addr.s_addr = 0; //inet_addr("127.255.255.255");
	if(bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0){
		printf("BIND\n");
		shutdown(fd, SHUT_RDWR);
		return(-1);
	}
	mreq.imr_multiaddr.s_addr = inet_addr("224.0.0.251");
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	if(setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0){
		printf("MULTICAST\n");
		shutdown(fd, SHUT_RDWR);
		return(-1);
	}
	while(1){
		len = sizeof(struct sockaddr);
		l = recvfrom(fd, &buffer[0], 1024, 0, (struct sockaddr *)&addr, &len);
		if(l < 0){
			printf("recvfrom: %s\n", strerror(errno));
			usleep(1000);
		}else if(l > 0){
			printf("%s: ", inet_ntoa(addr.sin_addr));
			printf("PORT: %u\n", ntohs(addr.sin_port));
			for(i = 0; i < l; i++){
				printf(" %02X", (unsigned int)buffer[i]);
		//		printf("%c", (unsigned int)buffer[i]);
			}
			printf("\n");
			fflush(stdout);
			mdns_recv_udp(buffer, l);
		}
	}
	shutdown(fd, SHUT_RDWR);
	return(-1);
}

void mdns_recv_query_a(const void *recv_buffer, uint16_t len, uint16_t name){
	in_addr_t addr;
	uint32_t server_ip;

	printf("QUERY A\n");
	if(!dns_name_isequal(recv_buffer, len, name, server_name, strlen(server_name)))return;
	printf("QUERY A1\n");
	if(iface_addr_get(interface_name, &addr) < 0)return;
	server_ip = ntohl(addr);
	printf("QUERY A2: %08X\n", (unsigned int)server_ip);
	mdns_send_answer_a(server_name, server_ip);
}

void mdns_recv_query_ptr(const void *recv_buffer, uint16_t len, uint32_t ip){
	in_addr_t addr;
	uint32_t server_ip;

	printf("QUERY PTR\n");
	if(iface_addr_get(interface_name, &addr) < 0)return;
	server_ip = ntohl(addr);
	if(ip != server_ip)return;
	printf("QUERY PTR1\n");
	mdns_send_answer_ptr(server_ip, server_name);
}

void mdns_send(const void *send_buffer, uint16_t len){
	struct sockaddr_in socket_address;
	int i;

	memset(&socket_address, 0, sizeof(socket_address));
	socket_address.sin_family = AF_INET;
	socket_address.sin_addr.s_addr = inet_addr("224.0.0.251");
	socket_address.sin_port = htons(MDNS_DEST_PORT);

	i = sendto(fd, send_buffer, len, 0, (struct sockaddr *)&socket_address, sizeof(socket_address));
	printf("SEND: %d\n", i);
}

static int iface_addr_get(const char *iface, in_addr_t *addr){
	int fd, retval;
	struct ifreq ifr;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if(fd < 0)return(-1);
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);
	retval = ioctl(fd, SIOCGIFADDR, &ifr);
	if(!retval)*addr = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr.s_addr;
	shutdown(fd, SHUT_RDWR);
	close(fd);
	return(retval);
}

static int iface_mac_get(const char *iface, void *mac){
	int fd, retval;
	struct ifreq ifr;
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if(fd < 0)return(-1);
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);
	retval = ioctl(fd, SIOCGIFHWADDR, &ifr);
	if(!retval)memcpy(mac, ifr.ifr_addr.sa_data, 6);
	shutdown(fd, SHUT_RDWR);
	close(fd);
	return(retval);
}

