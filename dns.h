#ifndef _DNS_H_
#define _DNS_H_

#include <stdint.h>

#define DNS_HEADER_LEN                12

#define DNS_SECTION_ERROR             0
#define DNS_SECTION_QUESTION          1
#define DNS_SECTION_ANSWER            2
#define DNS_SECTION_AUTHORITY         3
#define DNS_SECTION_ADDITIONAL        4

#define DNS_TYPE_A                    0x0001
#define DNS_TYPE_PTR                  0x000C

#define dns_id(buf)                   (((uint16_t)((uint8_t *)(buf))[0] << 8) | (((uint8_t *)(buf))[1] << 0))
#define dns_flags(buf)                (((uint16_t)((uint8_t *)(buf))[2] << 8) | (((uint8_t *)(buf))[3] << 0))
#define dns_qdcount(buf)              (((uint16_t)((uint8_t *)(buf))[4] << 8) | (((uint8_t *)(buf))[5] << 0))
#define dns_ancount(buf)              (((uint16_t)((uint8_t *)(buf))[6] << 8) | (((uint8_t *)(buf))[7] << 0))
#define dns_nscount(buf)              (((uint16_t)((uint8_t *)(buf))[8] << 8) | (((uint8_t *)(buf))[9] << 0))
#define dns_arcount(buf)              (((uint16_t)((uint8_t *)(buf))[10] << 8) | (((uint8_t *)(buf))[11] << 0))

#define dns_id_set(buf, id)           do{ ((uint8_t *)(buf))[0] = (((uint16_t)(id) >> 8) & 0xFF); ((uint8_t *)(buf))[1] = (((uint16_t)(id) >> 0) & 0xFF); }while(0)
#define dns_flags_set(buf, flags)     do{ ((uint8_t *)(buf))[2] = (((uint16_t)(flags) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint16_t)(flags) >> 0) & 0xFF); }while(0)
#define dns_qdcount_set(buf, qdcount) do{ ((uint8_t *)(buf))[4] = (((uint16_t)(qdcount) >> 8) & 0xFF); ((uint8_t *)(buf))[5] = (((uint16_t)(qdcount) >> 0) & 0xFF); }while(0)
#define dns_ancount_set(buf, ancount) do{ ((uint8_t *)(buf))[6] = (((uint16_t)(ancount) >> 8) & 0xFF); ((uint8_t *)(buf))[7] = (((uint16_t)(ancount) >> 0) & 0xFF); }while(0)
#define dns_nscount_set(buf, nscount) do{ ((uint8_t *)(buf))[8] = (((uint16_t)(nscount) >> 8) & 0xFF); ((uint8_t *)(buf))[9] = (((uint16_t)(nscount) >> 0) & 0xFF); }while(0)
#define dns_arcount_set(buf, arcount) do{ ((uint8_t *)(buf))[10] = (((uint16_t)(arcount) >> 8) & 0xFF); ((uint8_t *)(buf))[11] = (((uint16_t)(arcount) >> 0) & 0xFF); }while(0)

#define dns_uint16_set(buf, value)    do{ ((uint8_t *)(buf))[0] = (((uint16_t)(value) >> 8) & 0xFF); ((uint8_t *)(buf))[1] = (((uint16_t)(value) >> 0) & 0xFF); }while(0)
#define dns_uint32_set(buf, value)    do{ ((uint8_t *)(buf))[0] = (((uint32_t)(value) >> 24) & 0xFF); ((uint8_t *)(buf))[1] = (((uint32_t)(value) >> 16) & 0xFF); ((uint8_t *)(buf))[2] = (((uint32_t)(value) >> 8) & 0xFF); ((uint8_t *)(buf))[3] = (((uint32_t)(value) >> 0) & 0xFF); }while(0)

struct dns_section_struct{
	uint8_t section_type;
	uint16_t name;
	uint16_t type;
	uint16_t class;
	uint32_t ttl;
	uint16_t rlength;
	uint16_t rdata;
	uint8_t *buf;
	uint16_t size;
	uint16_t next;
	uint8_t section_num;
};

struct dns_name_struct{
	uint8_t valid;
	uint8_t length;
	uint16_t label;
	uint8_t *buf;
	uint16_t size;
	uint16_t next;
};

typedef struct dns_section_struct dns_section_t;
typedef struct dns_name_struct dns_name_t;

void dns_section_first(const void *buf, uint16_t size, dns_section_t *section);
uint8_t dns_section_valid(dns_section_t *section);
void dns_section_next(dns_section_t *section);

void dns_name_first(const void *buf, uint16_t size, uint16_t loc, dns_name_t *name);
uint8_t dns_name_valid(dns_name_t *name);
void dns_name_next(dns_name_t *name);

uint8_t dns_name_isequal(const void *buffer, uint16_t buffer_len, uint16_t name_loc, const char *str, uint16_t len);
uint16_t dns_name_encode(void *dest, uint16_t maxlen, const char *str, uint16_t len);

#endif

