#include "dns.h"
#include <ctype.h>

// http://www.zytrax.com/books/dns/ch15/

#define dns_uint16(buf) (((uint16_t)((uint8_t *)(buf))[0] << 8) | (((uint8_t *)(buf))[1] << 0))

void dns_section_first(const void *buf, uint16_t size, dns_section_t *section){
	section->buf = (uint8_t *)buf;
	section->size = size;
	section->next = 12;
	section->section_num = 0;
	dns_section_next(section);
}

uint8_t dns_section_valid(dns_section_t *section){
	return(section->section_type != DNS_SECTION_ERROR);
}

void dns_section_next(dns_section_t *section){
	uint8_t section_type, l;
	section->section_type = DNS_SECTION_ERROR;
	if(section->next >= section->size)return;
	section->section_num++;
	if(section->section_num <= dns_qdcount(section->buf)){
		section_type = DNS_SECTION_QUESTION;
	}else if(section->section_num <= (dns_qdcount(section->buf) + dns_ancount(section->buf))){
		section_type = DNS_SECTION_ANSWER;
	}else if(section->section_num <= (dns_qdcount(section->buf) + dns_ancount(section->buf) + dns_nscount(section->buf))){
		section_type = DNS_SECTION_AUTHORITY;
	}else if(section->section_num <= (dns_qdcount(section->buf) + dns_ancount(section->buf) + dns_nscount(section->buf) + dns_arcount(section->buf))){
		section_type = DNS_SECTION_ADDITIONAL;
	}else{
		return;
	}
	section->name = section->next;
	do{
		l = section->buf[section->next];
		if(l & 0xC0){
			section->next += 1 + 1;
			l = 0;
		}else{
			section->next += 1 + l;
		}
		if(section->next >= section->size)return;
	}while(l != 0);
	if((section->next + 4) > section->size)return;
	section->type = dns_uint16(&section->buf[section->next]);
	section->class = dns_uint16(&section->buf[section->next + 2]);
	section->next += 4;
	if(section_type == DNS_SECTION_QUESTION){
		section->section_type = section_type;
		section->ttl = 0;
		section->rlength = 0;
		section->rdata = 0;
		return;
	}
	if((section->next + 6) > section->size)return;
	section->ttl = (((uint32_t)section->buf[section->next] << 24) | ((uint32_t)section->buf[section->next + 1] << 16) | ((uint32_t)section->buf[section->next + 2] << 8) | ((uint32_t)section->buf[section->next + 3] << 0));
	section->next += 4;
	section->rlength = dns_uint16(&section->buf[section->next]);
	section->next += 2;
	section->rdata = section->next;
	section->next += section->rlength;
	if(section->next > section->size)return;
	section->section_type = section_type;
}

void dns_name_first(const void *buf, uint16_t size, uint16_t loc, dns_name_t *name){
	name->buf = (uint8_t *)buf;
	name->size = size;
	name->next = loc;
	dns_name_next(name);
}

uint8_t dns_name_valid(dns_name_t *name){
	return(name->valid);
}

void dns_name_next(dns_name_t *name){
	uint8_t l, loops;
	name->valid = 0;
	loops = 0;
restart:
	loops++;
	if(loops > 16)return;
	if(name->next >= name->size)return;
	l = name->buf[name->next];
	if(l & 0xC0){
		if((name->next + 1 + 1) >= name->size)return;
		name->next = (((uint16_t)(l & 0x3F)) << 8) | (((uint16_t)name->buf[name->next + 1]) << 0);
		goto restart;
	}
	if(!l)return;
	name->length = l;
	name->next += 1;
	name->label = name->next;
	name->next += l;
	if(name->next > name->size)return;
	name->valid = 1;
}

uint8_t dns_name_isequal(const void *buffer, uint16_t buffer_len, uint16_t name_loc, const char *str, uint16_t len){
	dns_name_t n;
	uint16_t s, i;
	s = 0;
	for(dns_name_first(buffer, buffer_len, name_loc, &n); dns_name_valid(&n); dns_name_next(&n)){
		if(s != 0){
			if((s >= len) || (str[s] != '.'))return(0);
			s++;
		}
		for(i = 0; i < n.length; i++){
			if((s >= len) || (toupper(((char *)buffer)[n.label + i]) != toupper(str[s])))return(0);
			s++;
		}
	}
	if(s != len)return(0);
	return(1);
}

uint16_t dns_name_encode(void *dest, uint16_t maxlen, const char *str, uint16_t len){
	uint16_t loc, lenloc, strloc;
	uint8_t l;
	if(!maxlen)return(0);
	if((maxlen < 3) || !len){
		((char *)dest)[0] = 0;
		return(1);
	}
	l = 0;
	lenloc = 0;
	loc = 1;
	for(strloc = 0; strloc < len; strloc++){
		if(str[strloc] == '.'){
			if(!l)break;
			((uint8_t *)dest)[lenloc] = l;
			l = 0;
			lenloc = loc;
			loc++;
		}else{
			if(l == 255)break;
			((char *)dest)[loc++] = str[strloc];
			l++;
		}
		if(loc >= (maxlen - 1))break;
	}
	if(l){
		((uint8_t *)dest)[lenloc] = l;
	}else{
		loc--;
	}
	((char *)dest)[loc++] = 0;
	return(loc);
}

