#include "mdns.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include "dns.h"

static uint8_t mdns_name2num(uint8_t *name, uint8_t len);

void mdns_recv_udp(const void *recv_buffer, uint16_t len){
	dns_section_t d;
	dns_name_t n;
	uint32_t ip;
	uint8_t i;

	for(dns_section_first(recv_buffer, len, &d); dns_section_valid(&d); dns_section_next(&d)){
		if(!(dns_flags(recv_buffer) & 0x8000) && (d.section_type == DNS_SECTION_QUESTION)){
			if(d.type == DNS_TYPE_A){
				mdns_recv_query_a(recv_buffer, len, d.name);
			}else if(d.type == DNS_TYPE_PTR){
				ip = 0;
				i = 0;
				for(dns_name_first(recv_buffer, len, d.name, &n); dns_name_valid(&n); dns_name_next(&n)){
					if(i == 0){
						ip |= ((uint32_t)mdns_name2num(&((uint8_t *)recv_buffer)[n.label], n.length) << 0);
					}else if(i == 1){
						ip |= ((uint32_t)mdns_name2num(&((uint8_t *)recv_buffer)[n.label], n.length) << 8);
					}else if(i == 2){
						ip |= ((uint32_t)mdns_name2num(&((uint8_t *)recv_buffer)[n.label], n.length) << 16);
					}else if(i == 3){
						ip |= ((uint32_t)mdns_name2num(&((uint8_t *)recv_buffer)[n.label], n.length) << 24);
					}
					i++;
				}
				if(i == 6){
					mdns_recv_query_ptr(recv_buffer, len, ip);
				}
			}
		}
	}
}

void mdns_send_answer_a(const char *name, uint32_t ip){
	uint16_t l;
	uint8_t mdns_send_udp_packet[1024];

	dns_id_set(mdns_send_udp_packet, 0);
	dns_flags_set(mdns_send_udp_packet, 0x8400);
	dns_qdcount_set(mdns_send_udp_packet, 0);
	dns_ancount_set(mdns_send_udp_packet, 1);
	dns_nscount_set(mdns_send_udp_packet, 0);
	dns_arcount_set(mdns_send_udp_packet, 0);
	l = DNS_HEADER_LEN;
	l += dns_name_encode(&mdns_send_udp_packet[l], 512, name, strlen(name));
	dns_uint16_set(&mdns_send_udp_packet[l], 0x0001); l += 2;
	dns_uint16_set(&mdns_send_udp_packet[l], 0x8001); l += 2;
	dns_uint32_set(&mdns_send_udp_packet[l], 300); l += 4;
	dns_uint16_set(&mdns_send_udp_packet[l], 4); l += 2;
	mdns_send_udp_packet[l++] = ((ip >> 24) & 0xFF);
	mdns_send_udp_packet[l++] = ((ip >> 16) & 0xFF);
	mdns_send_udp_packet[l++] = ((ip >> 8) & 0xFF);
	mdns_send_udp_packet[l++] = ((ip >> 0) & 0xFF);
	mdns_send(mdns_send_udp_packet, l);
}

void mdns_send_answer_ptr(uint32_t ip, const char *name){
	uint16_t l, name_len_loc, name_len;
	uint8_t mdns_send_udp_packet[1024];
	char buffer[32];

	dns_id_set(mdns_send_udp_packet, 0);
	dns_flags_set(mdns_send_udp_packet, 0x8400);
	dns_qdcount_set(mdns_send_udp_packet, 0);
	dns_ancount_set(mdns_send_udp_packet, 1);
	dns_nscount_set(mdns_send_udp_packet, 0);
	dns_arcount_set(mdns_send_udp_packet, 0);
	l = DNS_HEADER_LEN;
	snprintf(buffer, 32, "%u.%u.%u.%u.in-addr.arpa", (unsigned int)((ip >> 0) & 0xFF), (unsigned int)((ip >> 8) & 0xFF), (unsigned int)((ip >> 16) & 0xFF), (unsigned int)((ip >> 24) & 0xFF));
	l += dns_name_encode(&mdns_send_udp_packet[l], 512, buffer, strlen(buffer));
	dns_uint16_set(&mdns_send_udp_packet[l], 0x000C); l += 2;
	dns_uint16_set(&mdns_send_udp_packet[l], 0x8001); l += 2;
	dns_uint32_set(&mdns_send_udp_packet[l], 300); l += 4;
	name_len_loc = l; l += 2;
	name_len = dns_name_encode(&mdns_send_udp_packet[l], 512, name, strlen(name));
	dns_uint16_set(&mdns_send_udp_packet[name_len_loc], name_len);
	l += name_len;
	mdns_send(mdns_send_udp_packet, l);
}

static uint8_t mdns_name2num(uint8_t *name, uint8_t len){
	if(len == 1){
		return(name[0] - '0');
	}else if(len == 2){
		return(((name[0] - '0') * 10) + (name[1] - '0'));
	}else if(len == 3){
		return(((name[0] - '0') * 100) + ((name[1] - '0') * 10) + (name[2] - '0'));
	}
	return(0);
}

